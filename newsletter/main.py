from peewee import *
from telegram import *
from telegram.ext import *

from newsletter import config

db = SqliteDatabase('database.db')
updater = Updater(config.token)


class User(Model):
    id = AutoField()
    user_id = IntegerField(unique=True)
    abo = BooleanField(default=True)
    weekly = BooleanField(default=False)
    news = BooleanField(default=False)
    press = BooleanField(default=False)
    bot = BooleanField(default=False)
    admin = BooleanField(default=False)
    chat = BooleanField(default=False)
    blocked = BooleanField(default=False)

    class Meta:
        database = db


class Newsletter(Model):
    id = AutoField
    user_id = ForeignKeyField(User, backref="newsletters")
    category = TextField()
    text = TextField()
    date = DateTimeField()
    recipients = IntegerField()

    class Meta:
        database = db


class Message(Model):
    id = AutoField()
    user_id = ForeignKeyField(User, backref="messages")
    newsletter_id = IntegerField()
    message_id = IntegerField()

    class Meta:
        database = db


db.create_tables([User, Newsletter, Message])

reply_keyboard = [['/kategorien', '/stop']]
reply_markup2 = ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=False, resize_keyboard=True)


def start(bot, update):
    update.message.reply_text(
        "*Willkommen im FFF-Newsletter!*\nHier gibt es alle aktuellen Infos zu Fridays for Future.\nUnter Kategorien kannst du auswählen, welche Nachrichten du erhalten möchtest. Wenn du keine Nachrichten mehr empfangen möchtest, drücke auf Stopp.",
        reply_markup=reply_markup2, parse_mode="Markdown")
    try:
        user = User.select().where(User.user_id == update.effective_user.id).get()
        user.abo = True
        user.save()
    except:
        pass

    User.create(user_id=update.effective_user.id)


def stop(bot, update):
    user = User.select().where(User.user_id == update.effective_user.id).get()
    user.abo = False
    user.save()
    update.message.reply_text("Du wirst keine Newsletter mehr erhalten. Wenn du wieder welche kriegen willst, "
                              "sende /start")


def categories(bot, update):
    user = User.select().where(User.user_id == update.effective_user.id).get()

    keyboard = [[InlineKeyboardButton("Weekly ✅", callback_data='weekly'),
                 InlineKeyboardButton("News ✅", callback_data='news')],
                [InlineKeyboardButton("Presse ✅", callback_data='press'),
                 InlineKeyboardButton("Bot ✅", callback_data='bot')]]

    reply_markup = InlineKeyboardMarkup(keyboard)
    if user.abo:
        update.message.reply_text(
            "*Kategorien*\nDu kannst hier auswählen, aus welchen Kategorien du Nachrichten erhalten möchtest.\n\n📆 Weekly\n📂 Wöchentliche Zusammenfassung\n✉️ eine Nachricht pro Woche\n\n🗞 News\n📂 Aktuelle Neuigkeiten\n✉️ seltener, max. eine Nachricht pro Tag\n\n🎙 Presse\n📂 Presseinformationen\n✉️ seltener, je nach Tag und Aktivität\n\n🤖 Bot\n📂 Neuigkeiten zum Bot und zu neuen Funktionen\n✉️ selten",
            reply_markup=reply_markup, parse_mode="Markdown")
    else:
        update.message.reply_text("Bitte sende erst /start")


def help(bot, update):
    update.message.reply_text("Dieser Bot sendet den FridaysForFuture Germany Newsletter. Um ihn zu stoppen: /stop",
                              parse_mode="Markdown")


# def enable_or_disable(value, user_id):
#     user = User.select().where(User.user_id == user_id).get()
#     if getattr(user, value) is False:
#         setattr(user, value, True)
#         print(getattr(user, value))
#     else:
#         setattr(user, value, False)
#         print(getattr(user, value))
#     user.save()


def handlebutton(bot, update):
    print(update.effective_user.id)
    user = User.select().where(User.user_id == update.effective_user.id).get()
    if user.abo is True:
        if update.callback_query.data == "weekly":
            print("Jemand hat den Weekly Newsletter abboniert!")
            if user.weekly is True:
                user.weekly = False
            else:
                user.weekly = True

        if update.callback_query.data == "news":
            print("Jemand hat den News Newsletter abboniert!")
            if user.news is True:
                user.news = False
            else:
                user.news = True

        if update.callback_query.data == "press":
            print("Jemand hat den Presse Newsletter abboniert!")
            if user.press is True:
                user.press = False
            else:
                user.press = True

        if update.callback_query.data == "bot":
            print("Jemand hat den Bot Newsletter abboniert!")
            if user.bot is True:
                user.bot = False
            else:
                user.bot = True
        user.save()


updater.dispatcher.add_handler(CommandHandler('start', start))
updater.dispatcher.add_handler(CommandHandler('stop', stop))

updater.dispatcher.add_handler(CommandHandler('kategorien', categories))
updater.dispatcher.add_handler(CommandHandler('k', categories))

updater.dispatcher.add_handler(CommandHandler('help', help))

updater.dispatcher.add_handler(CallbackQueryHandler(handlebutton))

updater.start_polling()
updater.idle()
